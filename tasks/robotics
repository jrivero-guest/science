Task: Robotics
Install: false
Description: Debian Robotics packages
 This metapackage is part of the Debian Pure Blend "Debian Science"
 and installs packages related to Robotics.
 .
 You might also be interested in the science-engineering metapackage.

Recommends: rtai, xenomai-runtime

Recommends: morse-simulator

Recommends: choreonoid

Suggests: arduino-mk

Recommends: liburdfdom-tools

Recommends: solid
WNPP: 500616
Homepage: http://www.dtecta.com
License: GPL, QPL
Language: C++
Pkg-Description: Software library for collision detection of geometric objects in 3D space.
 Collision detection is the process of detecting pairs of objects that
 are intersecting or are within a given proximity of each other. SOLID
 contains operations for performing intersection tests and proximity
 queries on a wide variety of shape types, including: deformable triangle
 meshes, boxes, ellipsoids, and convex polyhedra.
 .
 Since it exploits temporal coherence in a number of ways, SOLID is
 especially useful for detecting collisions between objects that move
 smoothly over time. The motions of objects are controlled by the client
 application, and are not determined or affected by SOLID.
 .
 Although it can be used for physics-based simulations, SOLID is not a
 physics engine by itself. SOLID leaves it up to the application
 programmer how the laws of physics are implemented.

Recommends: robot-player

Recommends: d-collide
Homepage: http://d-collide.ematia.de/
License: BSD
Responsible: Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Pkg-Description: D-Collide is a real-time collision detection library aimed
 at performance and accuracy, that supports rigid objects as well as
 deformables - such as cloth - including self-collision detection.

Recommends: rtnet
Homepage: http://www.rtnet.org
License: GPL
Responsible: Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Pkg-Description: hard real-time network protocol stack
 RTnet is an Open Soure hard real-time network protocol stack
 for Xenomai and RTAI (real-time Linux extensions). It makes use of standard
 Ethernet hardware and supports several popular NIC chip sets, including
 Gigabit Ethernet. Moreover, Ethernet-over-1394 support is available based on
 the RT-FireWire protocol stack.

Recommends: orca

Recommends: roboop
Homepage: http://www.cours.polymtl.ca/roboop/
License: GPL
Responsible: Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Pkg-Description: synthesis, and simulation of robotic manipulator models
 ROBOOP is a C++ robotics object oriented programming toolbox
 suitable for synthesis, and simulation of robotic manipulator models in an
 environment that provides ``MATLAB like'' features for the treatment of
 matrices. Its is a portable tool that does not require the use of commercial
 software.

Recommends: orocos-rtt
Homepage: http://www.orocos.org/rtt
License: GPL + runtime exception
Responsible: Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Pkg-URL: http://svn.fmtc.be/debian
Pkg-Description: Orocos Real-Time Toolkit
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.

Recommends: orocos-ocl
Homepage: http://www.orocos.org/ocl
License: LGPL
Responsible: Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Pkg-URL: http://svn.fmtc.be/debian
Pkg-Description: Orocos Component Library
 The Orocos Component Library uses the Real-Time Toolkit (RTT)
 for constructing all its components. Some components use the Kinematics and
 Dynamics Library (KDL), the Bayesian Filtering Library (BFL) or other
 libraries.

Recommends: ros-desktop-full,
         ros-desktop,
         ros-perception,
         ros-simulators,
         ros-robot,
         ros-robot-state-publisher,
         rosdiagnostic,
         joint-state-publisher

Suggests: joint-state-publisher-gui

Recommends: ros-opencv-apps

Recommends: openrtm
Homepage: http://www.openrtm.org/
Responsible: Thomas Moulard <thomas.moulard@gmail.com>
WNPP: 695240
License: LGPL-3
Pkg-Description: OpenRTM robotics middleware
 OpenRTM is a robotics middleware. It provides C++, Python, Java
 libraries to enable robotics component development and also
 integrates with Eclipse.  This middleware is OMG RTC standard
 compliant and is used by the robotics community.

Recommends: openhrp
Homepage: http://www.openrtp.jp/openhrp3/
Responsible: Thomas Moulard <thomas.moulard@gmail.com>
WNPP: 695656
License: EPL (Eclipse Public License)
Pkg-Description: OpenHRP robotics simulator
 OpenHRP3 (Open Architecture Human-centered Robotics Platform version
 3) is an integrated software platform for robot simulations and
 software developments. It allows the users to inspect an original
 robot model and control program by dynamics simulation. In addition,
 OpenHRP3 provides various software components and calculation
 libraries that can be used for robotics related software
 developments.

Recommends: gazebo9

Recommends: openrave
Homepage: http://openrave.org/
License: LGPL-3
Pkg-Description: OpenRAVE robotics platform
 OpenRAVE provides an environment for testing, developing, and
 deploying motion planning algorithms in real-world robotics
 applications. The main focus is on simulation and analysis of
 kinematic and geometric information related to motion
 planning. OpenRAVE's stand-alone nature allows is to be easily
 integrated into existing robotics systems.
 .
 It provides many command line tools to work with robots and planners,
 and the run-time core is small enough to be used inside controllers
 and bigger frameworks. An important target application is industrial
 robotics automation.

Suggests: ompl-demos
WNPP: 706133

Suggests: libcoin-runtime, vtk7

Suggests: octave, gnuplot

Recommends: octomap-tools, octovis


Recommends: aseba
